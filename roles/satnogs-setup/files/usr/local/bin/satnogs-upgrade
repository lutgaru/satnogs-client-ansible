#!/bin/sh -e
#
# SatNOGS client system upgrade script
#
# Copyright (C) 2019-2020 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. /etc/default/satnogs-setup

BOOTSTRAP_STAMP="$HOME/.satnogs/.bootstrapped"
INSTALL_STAMP="$HOME/.satnogs/.installed"
LATEST_CODENAME="buster"
SED_SOURCES_SUBS="s/stretch/buster/
s/Raspbian_9\\.0/Raspbian_10/
s/Debian_9\\.0/Debian_10/"
LC_ALL=C

export LC_ALL

upgrade() {
	apt-get update
	if apt-get -y -q -s dist-upgrade | grep -q "^The following packages will be upgraded:$" ; then
		apt-get dist-upgrade
		apt-get autoremove
		apt-get autoclean
	fi
}

release_upgrade() {
	for _sub in $SED_SOURCES_SUBS; do
		sed -i "$_sub" /etc/apt/sources.list.d/*.list /etc/apt/sources.list
	done
	apt-get update
	apt-get dist-upgrade
	apt-get autoremove
	apt-get autoclean
}

upgrade
if [ "$(lsb_release -c -s)" != "$LATEST_CODENAME" ]; then
	if whiptail \
		   --yesno \
		   --defaultno \
		   --backtitle "SatNOGS client system upgrade utility" \
		   --title "New distribution release available!" \
		   "Do you want to upgrade release now?

WARNING: Release upgrades can consume
a lot of time, network data and storage!
" 0 0; then
		release_upgrade
		rm -f "$BOOTSTRAP_STAMP" "$INSTALL_STAMP"
	fi
fi
